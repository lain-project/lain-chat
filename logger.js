const winston = require('winston');

const {LoggingWinston} = require('@google-cloud/logging-winston');

const loggingWinston = new LoggingWinston({
    projectId: process.env.GOOGLE_PROJECT_ID,
});

const logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console(),
        // Add Cloud Logging
        loggingWinston,
    ],
});

module.exports = {logger}
