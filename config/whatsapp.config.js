require('dotenv').config()
module.exports = {
    bussinessId: process.env.WHATSAPP_BUSINESS_ID,
    phoneId: process.env.WHATSAPP_PHONE_NUMBER_ID ,
    wabaId: process.env.WHATSAPP_WABA_ID,
    version: process.env.WHATSAPP_VERSION,
    token: process.env.WHATSAPP_TOKEN,
}
