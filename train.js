const { client } = require("./database");
const { types } = require("pg");
const { executeQueries, executeQuery } = require("./v2");

const planesDB = async () => {
  const query = `SELECT * FROM study_plans`;
  const cl = await client();
  const result = await cl.query(query);
  const studiesPlans = result.rows.map((plan) => plan.code + " - " + plan.name);
  return studiesPlans;
};

const arrayToString = (elements) =>
  elements.reduce((prev, curr) => prev + "\n" + curr, "");

const materiasDB = async (plan) => {
  console.log(plan);
  const query =
    "select  s.* from subjects s join study_plans_years spy on s.study_plan_year = spy .id join study_plans sp on spy.study_plan_id = sp.id where sp.code=$1";
  const cl = await client();
  const result = await cl.query(query, [plan]);
  const subjects = result.rows.map((materia) => materia.name);
  return subjects;
};

const train = async (req, res) => {
  const materias = await materiasDB();
  res.json({
    materias,
  });
};

const msg = async (req, res) => {
  const msg = req.body.msg;
  const userId = req.body.userId;
  const context = req.body.context
  const result = await executeQuery(userId, msg, context);
  const response = await handleResponse(result);
  res.json(response);
};

const handleResponse = async (queryResult) => {
  const action = queryResult.action || "";
  console.log(queryResult);

  switch (action) {
    case "Planes.action":
      const planes = await planesDB();
      const response =
        `Los planes de estudio existentes son: ` + arrayToString(planes);
      return response;
    case "elegirPlan.action":
      const parameters =
        queryResult.parameters.fields.planDeEstudio.stringValue;
      const materias = await materiasDB(parameters);
      return (
        `Las materias que posee el plan de estudio son: ` +
        arrayToString(materias)
      );
    case "obtenerMaterias.action":
      return queryResult;
    default:
      return queryResult.fulfillmentText;
  }
};

module.exports = { train, msg };
