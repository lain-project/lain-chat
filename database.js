const { Pool } = require("pg");

const client = async () => {
  try {
    const connectionString = process.env.DATABASE_URL;
    const pool = new Pool({
      connectionString,
      application_name: "$ docs_simplecrud_node-postgres",
    });
    const client = await pool.connect();
    return client;
  } catch (error) {
    console.error(error);
  }
};

module.exports = { client };
