const ERROR_MESSAGES = {
    BIENVENIDA: 'No se pudo obtener mensaje de bienvenida',
    OBTENER_PLANES: 'No se pudieron obtener planes de estudio',
    ELEGIR_PLAN: 'No se pudieron obtener las materias del plan',
    ELEGIR_MATERIA: 'No se pudo obtener la informacion de la materia',
    OBTENER_UNIDADES: 'No se pudieron obtener las unidades de la materia',
    OBTENER_DOCENTES: 'No se pudieron obtener los docentes de la materia',
    OBTENER_PROPUESTA: 'No se pudieron obtener la propuesta de la materia',
    OBTENER_CORRELATIVA: 'No se pudieron obtener las correlativas de la materia',

}


module.exports = {ERROR_MESSAGES}
