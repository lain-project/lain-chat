const CONTEXTOS = {
    PLAN: 'contexto_planes',
    MATERIA: 'contexto_materias'

}

const PARAMETROS = {
    planDeEstudio: 'planDeEstudio',
    materia: 'materia'
}

module.exports = {CONTEXTOS, PARAMETROS}
