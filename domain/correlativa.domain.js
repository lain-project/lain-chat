const CURSAR = {
    NO_APLICA: "0",
    REGULARIZADA: "1",
    APROBADA: "2",
}

const FINAL = {
    NO_APLICA: "0",
    APROBADA: "1",
}

const TIPO_CORRELATIVA = {
    REGULARIZADA_CURSAR: 'Regularizada para cursar',
    REGULARIZADA_Y_APROBADA: 'Regularizada para cursar y aprobada para rendir el final',
    APROBADA_CURSAR: 'Aprobado el final para cursar',
    APROBADA_CURSAR_APROBADA_FINAL: 'Aprobado el final para cursar y rendir el final',
    APROBADA_FINAL: 'Aprobado el final para rendir el final',
}

module.exports = {
    CURSAR,
    FINAL,
    TIPO_CORRELATIVA
}
