require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const app = express().use(bodyParser.json())
const cors = require('cors')
const port = process.env.PORT || 3002
const {processRequest} = require('./services/chat.service')
const {trainDialog, getEntities, createEntities} = require("./services/entity.service");

app.use(cors({
    origin: '*'
}))
app.listen(port, () => console.log(`Aplicacion corriendo en puerto ${port}`))

app.get("/", (req,res) => res.send("Chatbot Lain "))

app.post("/msg", async(req, res) => await processRequest(req, res))

app.get("/entidades", async(req, res) => await getEntities(req, res))

app.post("/entidades", async(req, res) => await createEntities(req, res))

app.post("/train", async(req, res) => await trainDialog(req, res))
