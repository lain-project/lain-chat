const {
    obtenerPlanesRepo,
    obtenerDocentesRepo,
    obtenerMateriasRepo,
    obtenerUnidadesRepo,
    obtenerPropuestaRepo,
    obtenerCorrelativasRepo, guardarChatRepo
} = require('../database/repository')
const {obtenerContexto, obtenerParametro} = require('./contexto.service')
const {CONTEXTOS, PARAMETROS} = require('../domain/contexto.domain')
const {sendMessage} = require('./whatsapp.service')
const {logger} = require("../logger");
const {ERROR_MESSAGES} = require("../domain/mensajes.domain");

async function bienvenidaMessage(response) {
    return response.text || ERROR_MESSAGES.BIENVENIDA;
}

async function obtenerPlanesMessage(response) {
    try {
        const planes = await obtenerPlanesRepo()
        return `A continuacion se listan los planes de estudio${planes}`
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para obtener planes`)
        return ERROR_MESSAGES.OBTENER_PLANES
    }

}

async function elegirPlanMessage(response) {
    try {
        const contexto = obtenerContexto(response.contexts, CONTEXTOS.PLAN)
        const codigoPlan = obtenerParametro(contexto, PARAMETROS.planDeEstudio)
        const materias = await obtenerMateriasRepo(codigoPlan)
        return `Las materias del plan ${codigoPlan} son:${materias}`
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para elegir plan`)
        return ERROR_MESSAGES.ELEGIR_PLAN
    }

}

async function elegirMateria(response) {
    try {
        const contexto = obtenerContexto(response.contexts, CONTEXTOS.MATERIA)
        const materia = obtenerParametro(contexto, PARAMETROS.materia)
        return `De la materia ${materia}, puede ver: \n - Unidad\n - Docentes \n - Correlativas\n - Propuesta`
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para elegir materia`)
        return ERROR_MESSAGES.ELEGIR_MATERIA
    }
}

async function obtenerUnidades(response) {
    try {
        const contexto = obtenerContexto(response.contexts, CONTEXTOS.MATERIA)
        const plan = obtenerParametro(contexto, PARAMETROS.planDeEstudio)
        const materia = obtenerParametro(contexto, PARAMETROS.materia)
        const unidades = await obtenerUnidadesRepo(plan, materia)
        return `Las unidades de ${materia} son: ${unidades}`
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para obtener unidades`)
        return ERROR_MESSAGES.OBTENER_UNIDADES
    }
}

async function obtenerDocentes(response) {
    try {
        const contexto = obtenerContexto(response.contexts, CONTEXTOS.MATERIA)
        const plan = obtenerParametro(contexto, PARAMETROS.planDeEstudio)
        const materia = obtenerParametro(contexto, PARAMETROS.materia)
        const docentes = await obtenerDocentesRepo(plan, materia)
        return `Los docentes de ${materia} son: ${docentes}`
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para obtener los docentes`)
        return ERROR_MESSAGES.OBTENER_DOCENTES
    }

}

async function obtenerPropuesta(response) {
    try {
        const contexto = obtenerContexto(response.contexts, CONTEXTOS.MATERIA)
        const plan = obtenerParametro(contexto, PARAMETROS.planDeEstudio)
        const materia = obtenerParametro(contexto, PARAMETROS.materia)
        const propuesta = await obtenerPropuestaRepo(plan, materia)
        return propuesta
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para obtener la propuesta`)
        return ERROR_MESSAGES.OBTENER_PROPUESTA
    }

}

function obtenerContextoPlanMateria(response) {
    const contexto = obtenerContexto(response.contexts, CONTEXTOS.MATERIA)
    const plan = obtenerParametro(contexto, PARAMETROS.planDeEstudio)
    const materia = obtenerParametro(contexto, PARAMETROS.materia)
    return {plan, materia}
}

async function obtenerCorrelativas(response) {
    try {
        const {plan, materia} = obtenerContextoPlanMateria(response)
        const correlativas = await obtenerCorrelativasRepo(plan, materia)
        return correlativas
    } catch (error) {
        logger.error(`HANDLER: error al generar mensaje para obtener la propuesta`)
        return ERROR_MESSAGES.OBTENER_CORRELATIVA
    }

}

const handlerAction = {
    'bienvenida.action': bienvenidaMessage,
    'obtenerPlanes.action': obtenerPlanesMessage,
    'elegirPlan.action': elegirPlanMessage,
    'obtenerMaterias.action': elegirPlanMessage,
    'elegirMateria.action': elegirMateria,
    'obtenerUnidades.action': obtenerUnidades,
    'obtenerDocentes.action': obtenerDocentes,
    'obtenerPropuesta.action': obtenerPropuesta,
    'obtenerCorrelativas.action': obtenerCorrelativas,
}

async function handlerResponse(response, userId, token, question) {

    let message = 'No se pudo entender el mensaje'
    if (response.action) {
        message = await handlerAction[response.action](response)
    }

    try {
        await sendMessage(userId, message, token)
        const chat = buildChat(question, message, userId, response.action)
        await guardarChatRepo(chat)
        return message
    } catch (error) {
        logger.error(`HANDLER: Se encontro error al enviar el msg`)
        logger.error('handlerResponse: error al manejar el mensaje')
        logger.error(error)
        return error;
    }
}


function buildChat(pregunta, respuesta, telefono, intencion) {
    return {pregunta, respuesta, telefono, intencion}
}

module.exports = {handlerResponse}
