const axios = require('axios')
const configWP = require('../config/whatsapp.config')
const {logger} = require("../logger");

const URL = `https://graph.facebook.com/${configWP.version}/${configWP.phoneId}/messages`

async function sendMessage(numero, mensaje, token) {

    try{
        logger.warn(`Se procede a enviar el mensaje ${mensaje}', al numero ${numero}`)
        logger.warn(`A la url ${URL}`)
        const apiKey = token || configWP.token
        const headers = { 'Authorization': `Bearer ${apiKey}`, 'Content-Type': 'application/json' }
        logger.warn(`con la configuracion ${JSON.stringify(headers)}`)
        const body = JSON.stringify(buildBody(numero, mensaje))
        logger.warn(`body a enviar a wpp: ${JSON.stringify(body)}`)
        await axios.post(URL, body, {headers} )

    }catch (error){
        console.log(`WHATSAPP: Se encontro error al enviar el msg`)
        logger.error(`Se encontro un error al enviar el msg`)
        logger.error(error)
        throw error
    }
}

function buildBody(numero, mensaje) {
    logger.info(`Se procede a buildear mensaje: ${mensaje}`)
    logger.info(`Se procede a buildear numero: ${numero}`)
    return {
        messaging_product: "whatsapp",
        recipient_type: "individual",
        to: numero,
        type: "text",
        text: {
            preview_url: false,
            body: mensaje
        }
    }
}

module.exports = {sendMessage}
