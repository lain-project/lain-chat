const {processMessage} = require('./dialogflow.service')
const {handlerResponse} = require('./handler.service')

async function processRequest({body}, res) {
    const {msg, userId, token} = body;

    const response = await processMessage(userId, msg);
    const result = await handlerResponse(response, userId, token, msg)

    res.json({
        result
    });
};


module.exports = {processRequest}
