

function arrayToString(elements){
    return elements.reduce((prev, curr) => prev + "\n" + curr, "");
}


function buildErrorMessage(message) {
    return {
        text: message,
        action: ''
    }
}

module.exports = {arrayToString, buildErrorMessage}
