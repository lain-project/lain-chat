const dialogflow = require("@google-cloud/dialogflow");

const entityClient = new dialogflow.EntityTypesClient();
const agentPath = entityClient.projectAgentPath(process.env.GOOGLE_PROJECT_ID);
const request = { parent: agentPath };

async function getEntities(req, res) {
    const response = await entityClient.listEntityTypes(request)
    const result = response[0]
    return res.json(result)
}

async function trainDialog({body}, res){

    const request = { parent: agentPath, entityType: body}

    const response = await entityClient.updateEntityType(request)
    res.json({
        response
    })
}

async function createEntities({body}, res) {

    const request = { parent: agentPath, entityType: {}}

    const promises = body.map(entity => {
        request.entityType = entity
        return entityClient.updateEntityType(request)
    })

    const result = await Promise.all(promises)
    res.json({result})
}


module.exports = {trainDialog, getEntities, createEntities}
