

function obtenerContexto(contextos=[], nombre){
    return contextos.find( contexto => contexto.name.includes(nombre))
}

function obtenerParametro(contexto, nombre){
    const {parameters} = contexto;
    const {fields} = parameters
    return fields[nombre]?.stringValue
}


module.exports = {obtenerContexto, obtenerParametro}
