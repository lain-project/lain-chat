const dialogflow = require("@google-cloud/dialogflow");
const config = require("../config/dialogflow.config");
const {logger} = require("../logger");
const {buildErrorMessage} = require("./utils.service");

const sessionClient = new dialogflow.SessionsClient();

async function processMessage(sessionId, message){
    try {
        logger.info(`Se procede a procesar el mensaje`)
        let intentResponse = await detectIntent(
            sessionId,
            message,
        );
        return parseResponse(intentResponse.queryResult);
    } catch (error) {
        logger.error(`processMessage: Se encontro error al procesar el mensaje`)
        logger.error(error)
        return buildErrorMessage(message)
    }
}

async function detectIntent(sessionId, message) {
    try{
        const sessionPath = sessionClient.projectAgentSessionPath(
            config.GOOGLE_PROJECT_ID,
            sessionId
        );
        const request = buildQuery(sessionPath, message)
        const responses = await sessionClient.detectIntent(request);
        return responses[0];
    } catch (error) {
        logger.error(`detectIndent: error al detectar la intencion`)
        throw error
    }

}

function buildQuery (sessionPath, message) {
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: message,
                languageCode: config.DF_LANGUAGE_CODE,
            },
        },
        queryParams: {}
    };

    return request;
}

function parseResponse(responseDialogflow) {
    const text = responseDialogflow.fulfillmentMessages[0]?.text?.text[0]
    const contexts = responseDialogflow.outputContexts
    const parameters = responseDialogflow.parameters
    const action = responseDialogflow.action

    return {
        text,
        contexts,
        parameters,
        action
    }
}

module.exports = { processMessage };
