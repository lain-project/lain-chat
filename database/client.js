const { Pool } = require("pg");

const lainConnection = async () => {
    try {
        const connectionString = process.env.DATABASE_LAIN_URL;
        const pool = new Pool({
            connectionString,
            application_name: "$ lain",
        });
        const client = await pool.connect();
        return client;
    } catch (error) {
        console.error(`CLIENT: Error al conectarse a DB ${process.env.DATABASE_LAIN_URL}`);
        console.error(error);
        throw error
    }
};


const lainChatsConnection = async () => {
    try {
        const connectionString = process.env.DATABASE_LAIN_CHATS_URL;
        const pool = new Pool({
            connectionString,
            application_name: "$ lain chats",
        });
        const client = await pool.connect();
        return client;
    } catch (error) {
        console.error(`CLIENT: Error al conectarse a DB ${process.env.DATABASE_LAIN_CHATS_URL}`);
        console.error(error);
        throw error;
    }
};

module.exports = { lainConnection, lainChatsConnection };
