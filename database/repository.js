const {lainConnection, lainChatsConnection} = require('./client')
const {arrayToString} = require('../services/utils.service')
const {CURSAR, FINAL, TIPO_CORRELATIVA} = require("../domain/correlativa.domain");
const {logger} = require("../logger");

async function obtenerPlanesRepo() {
    try {
        const client = await lainConnection()
        const query = `SELECT * FROM study_plans`;
        const {rows} = await client.query(query);
        const studiesPlans = rows.map((plan) => plan.code + " - " + plan.name);
        return arrayToString(studiesPlans);
    } catch (error) {
        console.log(`REPOSITORY: error al obtener planes en base de datos`)
        console.log(error)
        throw error;
    }
}

async function obtenerMateriasDB(codigo) {
    try {
        const client = await lainConnection()
        const query = "select  s.* from subjects s join study_plans_years spy on s.study_plan_year = spy .id join study_plans sp on spy.study_plan_id = sp.id where sp.code=$1";
        const result = await client.query(query, [codigo]);
        return result.rows
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener materias del plan ${codigo} en base de datos`)
        logger.error(error)
        throw error
    }

}

async function obtenerMateriasRepo(codigo) {
    try {

        const result = await obtenerMateriasDB(codigo)
        const materias = result.map((materia) => materia.name);
        return arrayToString(materias);
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener materias del plan ${codigo} en base de datos`)
        logger.error(error)
        throw error
    }

}

async function obtenerUnidadesRepo(plan, materiaNombre) {
    try {

        const materias = await obtenerMateriasDB(plan)
        const materia = materias
            .find(materia => materia.name === materiaNombre)

        const client = await lainConnection()
        const query = "select u.* from unidades u join subjects s on u.materia_id  = s.id  where s.id=$1 order by numero"

        const result = await client.query(query, [materia.id])
        const unidades = result.rows.map(unidad => unidad.numero + ' - ' + unidad.nombre);
        return arrayToString(unidades)
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener unidades del plan ${plan} y la materia ${materiaNombre} en base de datos`)
        logger.error(error)
        throw error
    }


}

async function obtenerDocentesRepo(plan, materiaNombre) {
    try {
        const materias = await obtenerMateriasDB(plan)
        const materia = materias.find(materia => materia.name === materiaNombre)
        const client = await lainConnection()
        const query = "select p.* from docentes_materias dm join subjects s on dm.materia_id  = s.id join \"users\" u on dm.docente_id  = u.id " +
            "join profiles p on p.id  = u.profile_id \n" +
            "where s.id =$1"
        const result = await client.query(query, [materia.id])
        const docentes = result.rows.map(docente => `${docente.position.toUpperCase()} - ${docente.firstname}, ${docente.lastname}`)
        return arrayToString(docentes)
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener docentes del plan ${plan} y la materia ${materiaNombre} en base de datos`)
        logger.error(error)
        throw error
    }
}

async function obtenerPropuestaRepo(plan, materiaNombre) {
    try{
        const materias = await obtenerMateriasDB(plan)
        const materia = materias.find(materia => materia.name === materiaNombre)
        const client = await lainConnection()
        const query = "select * from propuestas p join subjects s on p.materia_id  = s.id where s.id = $1"
        const result = await client.query(query, [materia.id])
        const propuesta = result.rows[0]
        return buildPropuesta(propuesta)
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener propuesta del plan ${plan} y la materia ${materiaNombre} en base de datos`)
        logger.error(error)
        throw error
    }

}

function buildPropuesta(propuesta) {
    const contenidoMinimo = propuesta.contenido_minimo
        .replace(/<li>/g, ' - ')
        .replace(/<[^>]*>/g, '');
    const fundamentos = propuesta.fundamentos
        .replace(/<li>/g, ' - ')
        .replace(/<[^>]*>/g, '');
    const objetivos = propuesta.objetivos
        .replace(/<li>/g, ' - ')
        .replace(/<[^>]*>/g, '');
    const bibliografia = propuesta.bibliografia_general
        .replace(/<li>/g, ' - ')
        .replace(/<[^>]*>/g, '');

    return `*Contenido Minimo*\n${contenidoMinimo}\n\n*Fundamentos*\n${fundamentos}\n\n*Objetivos*\n${objetivos}\n\n*Bibliografia*\n${bibliografia}`
}

async function obtenerCorrelativasRepo(plan, materiaNombre) {
    try{
        const materias = await obtenerMateriasDB(plan)
        const materia = materias.find(materia => materia.name === materiaNombre)
        const client = await lainConnection()
        const query = "select s_inicial.name as mat_inicial, s_final.name as mat_final, c.* from correlativas c join subjects s_final on c.materia_final = s_final.id join subjects s_inicial on c.materia_inicial = s_inicial.id where s_final.id = $1 and c.estado = true"
        const result = await client.query(query, [materia.id])
        return correlativaMessage(result.rows)
    } catch (error) {
        logger.error(`REPOSITORY: error al obtener correlativas del plan ${plan} y la materia ${materiaNombre} en base de datos`)
        logger.error(error)
        throw error
    }
}

function correlativaMessage(correlativas = []) {
    const materia = correlativas[0].mat_final
    const correlativaMsg = correlativas.map(correlativa => '\n  - *' + correlativa.mat_inicial + '* - ' + armadoDeNombre(correlativa.cursar, correlativa.final) + '\n')
    return `Las correlativas de ${materia} son:\n${correlativaMsg.join("")}`

}

function armadoDeNombre(cursar, final) {
    const REGULARIZADA_CURSAR =
        cursar === CURSAR.REGULARIZADA &&
        final === FINAL.NO_APLICA;
    const REGULARIZADA_Y_APROBADA =
        cursar === CURSAR.REGULARIZADA &&
        final === FINAL.APROBADA;
    const APROBADA_CURSAR =
        cursar === CURSAR.APROBADA &&
        final === FINAL.NO_APLICA;
    const APROBADA_CURSAR_APROBADA_FINAL =
        cursar === CURSAR.APROBADA &&
        final === FINAL.APROBADA;
    const APROBADA_FINAL =
        cursar === CURSAR.NO_APLICA &&
        final === FINAL.APROBADA;

    if (REGULARIZADA_CURSAR) return TIPO_CORRELATIVA.REGULARIZADA_CURSAR;
    if (REGULARIZADA_Y_APROBADA)
        return TIPO_CORRELATIVA.REGULARIZADA_Y_APROBADA;
    if (APROBADA_CURSAR) return TIPO_CORRELATIVA.APROBADA_CURSAR;
    if (APROBADA_CURSAR_APROBADA_FINAL)
        return TIPO_CORRELATIVA.APROBADA_CURSAR_APROBADA_FINAL;
    if (APROBADA_FINAL) return TIPO_CORRELATIVA.APROBADA_FINAL;

    return '';
}

async function guardarChatRepo(chat) {
    try {
        const client = await lainChatsConnection()
        const query = "INSERT INTO preguntas (pregunta, intencion_detectada, respuesta, numero) VALUES($1, $2, $3, $4);"
        await client.query('BEGIN;');
        const result = await client.query(query, [chat.pregunta, chat.intencion, chat.respuesta, chat.telefono])
        await client.query('COMMIT;');
        return result.rows

    } catch (error) {
        logger.error(`REPOSITORY: error al guardar chat en base de datos`)
        logger.error(chat)
        logger.error(error)
        throw error
    }
}

module.exports = {
    obtenerPlanesRepo,
    obtenerMateriasRepo,
    obtenerUnidadesRepo,
    obtenerDocentesRepo,
    obtenerPropuestaRepo,
    obtenerCorrelativasRepo,
    guardarChatRepo
}

