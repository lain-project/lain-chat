const dialogflow = require("@google-cloud/dialogflow");
const uuid = require("uuid");
const config = require("./config-dialog");

const sessionClient = new dialogflow.SessionsClient();

async function detectIntent(
  projectId,
  sessionId,
  query,
  contexts,
  languageCode
) {
  // The path to identify the agent that owns the created intent.
  const sessionPath = sessionClient.projectAgentSessionPath(
    projectId,
    sessionId
  );

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: query,
        languageCode: languageCode,
      },
    },
  };

  if (contexts && contexts.length > 0) {
    request.queryParams = {
      contexts: contexts,
    };
  }
  console.log(request)

  const responses = await sessionClient.detectIntent(request);
  return responses[0];
}

async function executeQuery(userId, query = "planes", context = []){
  // Keeping the context across queries let's us simulate an ongoing conversation with the bot
  const projectId = config.GOOGLE_PROJECT_ID;
  const sessionId = userId
  console.log(context)
  try {
    let intentResponse = await detectIntent(
      projectId,
      sessionId,
      query,
      context,
      "es"
    );
    return intentResponse.queryResult;
  } catch (error) {
    console.log(error);
  }
}

module.exports = { executeQuery };
